package be.intec.demoIntelliJ.app;

import be.intec.demoIntelliJ.entiteit.Car;

public class CarApp {

    public static void main(String[] args) {

        // instantie aangemaakt van car
        Car nissan = new Car();

        System.out.println(nissan);

        // instantie aangemaakt van engine
        Car.Engine toyota = nissan.new Engine();

        toyota.setHp(80);

        Car.Interior interior = new Car().new Interior();
        Car.Exterior exterior = new Car().new Exterior();

        System.out.println(toyota);
        System.out.println(interior);
        System.out.println(exterior);

    }

}
