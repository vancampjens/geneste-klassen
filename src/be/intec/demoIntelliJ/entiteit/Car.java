package be.intec.demoIntelliJ.entiteit;

public class Car {

    int price;
    Engine engine;
    Interior interior;
    Exterior exterior;

    public Car() {
        engine = new Engine();
        interior = new Interior();
        exterior = new Exterior();
        this.price = engine.price + interior.price + exterior.price;
    }


    public void move() {
        engine.start();
        engine.run();
        engine.stop();
    }

    @Override
    public String toString() {
        return "Car{" +
                "price=" + price +
                ", engine=" + engine +
                ", interior=" + interior +
                ", exterior=" + exterior +
                '}';
    }

    public class Engine {
        private int hp;
        private int capacity;
        private int price;

        public Engine() {
            this(75, 6, 4000);
        }

        private Engine(int hp, int capacity, int price) {
            this.hp = hp;
            this.capacity = capacity;
            this.price = price;
        }

        public void run() {
            System.out.println("Engine is running.\n");
        }

        public void start() {
            System.out.println("Engine is starting.\n");
        }

        public void stop() {
            System.out.println("Engine is stopping.\n");
        }

        public int getHp() {
            return hp;
        }

        public void setHp(int hp) {
            this.hp = hp;
        }

        public int getCapacity() {
            return capacity;
        }

        public void setCapacity(int capacity) {
            this.capacity = capacity;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        @Override
        public String toString() {
            return "Engine{" +
                    "hp=" + hp +
                    ", capacity=" + capacity +
                    ", price=" + price +
                    '}';
        }
    }

    public class Interior {

        protected int price;

        public Interior() {
            this.price = 5000;
        }

        @Override
        public String toString() {
            return "Interior{" +
                    "price=" + price +
                    '}';
        }
    }

    public class Exterior {

        int price;

        public Exterior() {
            this.price = 7000;
        }

        @Override
        public String toString() {
            return "Exterior{" +
                    "price=" + price +
                    '}';
        }
    }



}
